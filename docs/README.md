---
hide:
  - navigation
  - toc
---

## I - Matériels necessaires 
 
 - Raspberry pi 3 B+ avec carte SD et alimentation
 - SOIC Clip Pomona 5252
 - 6 Jumper wires female/female
 - Thinkpad X200

## II - Préparer le RaspberryPi et le X200

Il vous faut créer une carte SD bootable avec l'outil raspberry image. Une fois que c'est fait, faite les commandes :

````
sudo apt-get update && sudo apt-get dist-upgrade && reboot
sudo apt-get update && sudo apt-get install libftdi1 libftdi-dev libusb-dev libpci-dev subversion
apt-get install python3-sphinx
````

Puis activer les options SSH, SPI et I2C dans les configurations du Raspberry.

Enfin, lancer la commande **ip -c a** dans le terminal et récupérer l'ip de l'interface **eth0**, souvent en 192.168.X.X.

Une fois que c'est fait, vous pouvez dégager le cable HDMI, le clavier et la souris du Raspberry car nous allons nous y connecter en SSH.


Retirer les 9 vis suivantes du PC :

![image](./Pictures/PC2.png)

Dégager clavier en le poussant vers le haut :

![image](./Pictures/PC5.png)

Puis en le soulevant pour le débrancher :

![image](./Pictures/PC6.png)

Une fois que c'est fait, déclipsez avec précaution le plastique noir et par la même occasion, répérez la pile ( souvent entourée d'un plastique jaune ) et débranchez là en tirant sur le prise blanche qui est en dessous :

![image](./Pictures/PC8.png)

Enfin, repérez la puce qui va nous être utile : 

![image](./Pictures/PC9.png)

Ici, un autocollant blanc est dessus, mais si vous l'enlevez, vous pourrez lire le nom de la puce. Je peux lire **MX25L6405D**. Il faut aussi répérer le nombre de " pins ". Si vous en avez 16 comme moi, il faudra le SOIC Clip Pomona 5252 16 pins. Si vous en avez 8, alors il vous faudra le SOIC Clip Pomona 5252 8 pins.

## III - Télécharger le nécessaire et mise en place

Il faut d'abord créer un repertoire x200 

````
tim@pi$ mkdir x200
tim@pi$ cd x200
````

Récupérer la version de flashrom patché pour les Macronix flash chip : [https://vimuser.org/hackrom.tar.xz](https://vimuser.org/hackrom.tar.xz)

```
tim@pi$ wget https://vimuser.org/hackrom.tar.xz
tim@pi$ tar -xvf hackrom.tar.xz
tim@raspberrypi:~/x200/hackrom/src cd x200/hackrom/src
```

```
tim@raspberrypi:~/x200/hackrom/src make CONFIG_ENABLE_LIBUSB0_PROGRAMMERS=no CONFIG_ENABLE_LIBUSB1_PROGRAMMERS=no CONFIG_ENABLE_LIBPCI_PROGRAMMERS=no

tim@raspberrypi:~/x200/hackrom/src sudo make install CONFIG_ENABLE_LIBUSB0_PROGRAMMERS=no CONFIG_ENABLE_LIBUSB1_PROGRAMMERS=no CONFIG_ENABLE_LIBPCI_PROGRAMMERS=no
```

Il faut aussi récupérer la version de la ROM libreboot que vous voulez, pour ma part libreboot-20230625_x200_8mb.tar.xz :
```
tim@raspberrypi:~/x200 wget https://www.mirrorservice.org/sites/libreboot.org/release/stable/20230625/roms/libreboot-20230625_x200_8mb.tar.xz
tim@raspberrypi:~/x200 tar -xvf libreboot-20230625_x200_8mb.tar.xz
```

On coupe le raspberry Pi, on débranche la source d'alimentation et on branche les jumper wire en suivant le code couleur :
![image](./Pictures/schema.png)

![image](./Pictures/rasp3.png)

![image](./Pictures/rasp2.jpg)

Une fois que c'est fait, on branche le SOIC Clip sur la puce ( la partie basse du schéma Pomona 5252 doit être vers la puce Intel ). On rebranche l'alimentation et on se connecte en SSH sur la machine.

Dans le repertoire hackrom, on va vérifier que la puce est détéctée. A chaque commande flashrom, il sera nécessaire de mettre le flag **--workaround-mx** :
```
tim@raspberrypi:~/x200/hackrom $ ./flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=32768 --workaround-mx
flashrom v0.9.9-unknown on Linux 6.1.21-v7+ (armv7l)
flashrom is free software, get the source code at https://flashrom.org

Calibrating delay loop... OK.
Found Macronix flash chip "MX25L6405" (8192 kB, SPI) on linux_spi.
Found Macronix flash chip "MX25L6405D" (8192 kB, SPI) on linux_spi.
Found Macronix flash chip "MX25L6406E/MX25L6408E" (8192 kB, SPI) on linux_spi.
Found Macronix flash chip "MX25L6436E/MX25L6445E/MX25L6465E/MX25L6473E" (8192 kB, SPI) on linux_spi.
Multiple flash chip definitions match the detected chip(s): "MX25L6405", "MX25L6405D", "MX25L6406E/MX25L6408E", "MX25L6436E/MX25L6445E/MX25L6465E/MX25L6473E"
Please specify which chip definition to use with the -c <chipname> option.
```

Maintenant que c'est ok, nous allons faire un backup de la " Factory ROM ' ( la ROM d'origine ) :

```
tim@raspberrypi:~/x200/hackrom $ sudo ./flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=32768 -r dump.bin --workaround-mx -c MX25L6405D
flashrom v0.9.9-unknown on Linux 6.1.21-v7+ (armv7l)
flashrom is free software, get the source code at https://flashrom.org

Calibrating delay loop... OK.
Found Macronix flash chip "MX25L6405D" (8192 kB, SPI) on linux_spi.
Reading flash... done.

tim@raspberrypi:~/x200/hackrom $ ls
dump.bin  flashrom  src
```

Puis le faire une deuxième fois :
```
rtim@raspberrypi:~/x200/hackrom $ sudo ./flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=32768 -r dump2.bin --workaround-mx -c MX25L6405D
flashrom v0.9.9-unknown on Linux 6.1.21-v7+ (armv7l)
flashrom is free software, get the source code at https://flashrom.org

Calibrating delay loop... OK.
Found Macronix flash chip "MX25L6405D" (8192 kB, SPI) on linux_spi.
Reading flash... done.
```

Et on va s'assurer que les .rom sont identiques :
```
rasp-libreboot@raspberrypi:~/x200/hackrom $ sha1sum dump*.bin
18b242e5173347eafaaff80f2ca028c9106d919a  dump2.bin
18b242e5173347eafaaff80f2ca028c9106d919a  dump.bin
```

Si tout ça l'est, on peut commencer :

```
tim@raspberrypi:~/x200/hackrom $ sudo ./flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=256 -w ../bin/x200_8mb/grub_x200_8mb_libgfxinit_corebootfb_frazerty_noblobs.rom --workaround-mx -c MX25L6405D
flashrom v0.9.9-unknown on Linux 6.1.21-v7+ (armv7l)
flashrom is free software, get the source code at https://flashrom.org
```

Il faut attendre un peu, si tout est ok, cela devrait se terminer par **Verifying flash... VERIFIED**  :
```
Calibrating delay loop... OK.
Found Macronix flash chip "MX25L6405D" (8192 kB, SPI) on linux_spi.
Reading old flash chip contents... done.
Erasing and writing flash chip... Erase/write done.
Verifying flash... VERIFIED.
```

Il n'y a plus qu'a démarrer et... :

![image1](./Pictures/x200-libreboot.png)

